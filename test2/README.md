## Check File Content
This library provide validation function based on provided configuration inside folder ./src/Check

To add new file type, create new file with same pattern:

* Type_A.php for validating Type_A.xls/xlsx
* Type_B.php for validating Type_B.xls/xlsx
* ...
* ....
* Type_X.php fir validating Type_X.xls/xlsx
