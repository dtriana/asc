<?php

namespace FileType\Check;

/**
 * Provide headers configuration for file Type_A
 */
class Type_A 
{

    /**
     * Header configuration
     * 
     * @var array 
     */
    private static $_headers = [
        "A" => "Field_A*",
        "B" => "#Field_B",
        "C" => "Field_C",
        "D" => "Field_D*",
        "E" => "Field_E*"
    ];

    /**
     * Get headers configuration accessible from outside
     * 
     * @return array
     */
    public static function getHeaders() 
    {
        return self::$_headers;
    }

}
