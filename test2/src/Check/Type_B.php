<?php

namespace FileType\Check;

/**
 * Provide headers configuration for file Type_B
 */
class Type_B 
{

    /**
     * Header configuration
     * 
     * @var array 
     */
    private static $_headers = [
        "A" => "Field_A*",
        "B" => "#Field_B"
    ];

    /**
     * Get headers configuration accessible from outside
     * 
     * @return array
     */
    public static function getHeaders() 
    {
        return self::$_headers;
    }

}
