<?php

namespace FileType;


/**
 * Class to check validity of a FileType
 * Based on configured headers
 */
class Check {

    /**
     * Configured headers
     * @var array 
     */
    private $headers;
    
    /**
     * String filename
     * @var string 
     */
    private $file;
    
    /**
     * Errors occurred when checking file
     * @var array 
     */
    private $errors = [];
    
    /**
     * Sheet object to be check
     * @var PhpOffice\PhpSpreadsheet\Style\Supervisor; 
     */
    private $excel;
    
    /**
     * Default sheet name to check
     * @var string 
     */
    private $defaultSheet = "Sheet1";
    
    /**
     * Properties of sheet to be check
     * @var array
     */
    private $sheetProperties;

    
    /**
     * When this class initiated, will check it's header
     * configuration, and prepare sheet object
     * 
     * @param string $file_name
     * @throws \Exception
     */
    public function __construct($file_name) 
    {
        $this->file = $file_name;
        $path_arr = pathinfo($file_name);
        $file_type = $path_arr['filename'] ?? "";
        $class_name = "FileType\\Check\\" . $file_type;

        if (class_exists($class_name)) {
            $this->headers = $class_name::getHeaders();
        } else {
            throw new \Exception("Type: " . $file_type . " Not Exists");
        }
        
        $this->prepareExcel();
        
    }

    /**
     * Prepared sheet object  will be validated with
     * related configuration
     */
    public function validate() 
    {
        $sheetData = $this->excel->getActiveSheet()->toArray(null, true, true, true);
        $sheetData[1] ?? $this->errors["no_header"] = "Header not exists";
        $headers = array_filter($sheetData[1]);
        count($headers) <> count($this->headers) &&
                $this->errors["invalid_header_count"] = "Invalid header column count";
        $headers !== $this->headers && 
                $this->errors["invalid_header_name"] = "Invalid header name or arrangement";
        
        $this->checkErrors();
        $methods = $this->collectMethod($headers);
        $maxcol =  $this->sheetProperties['totalColumns'];
        
        for($i = 2; $i < $maxcol; $i++)
        {
            $this->validateRow($methods, array_filter($sheetData[$i]), $i);
        }
        
        $this->printReport();
    }
    
    /**
     * Preparing sheet object and collecting sheet properties
     */
    private function prepareExcel()
    {
        $fileType    = \PhpOffice\PhpSpreadsheet\IOFactory::identify($this->file);
        $reader      = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($fileType);
        $reader->setLoadSheetsOnly($this->defaultSheet);
        
        $this->excel = $reader->load($this->file);
        $worksheetData = $reader->listWorksheetInfo($this->file);
        $index = array_search($this->defaultSheet, array_column($worksheetData, "worksheetName"));
        $this->sheetProperties = $worksheetData[$index];
    }
    
    
    /**
     * Prepare method to validate each cell based on
     * configured headers
     * 
     * @param array $headers
     * @return array
     */
    private function collectMethod($headers)
    {
        $methods = [];
        
        foreach ($headers as $key => $val)
        {
            $field_name = trim(str_replace(["#","*"], ["", ""], $val));
            if(strpos($val, "#") !== false){
                $methods[$key]['method'] = "noSpace";
                $methods[$key]['header'] = $field_name;
            } elseif(strpos($val, "*") !== false){
                $methods[$key]['method'] = "required";
                $methods[$key]['header'] = $field_name;
            }
        }
        
        return $methods;
    }
    
    /**
     * 
     * @param array $methods
     * @param array $data
     * @param int $row
     * @return void
     */
    private function validateRow($methods, $data, $row)
    {
        if(!count($data))
            return;
        
        foreach ($methods as $key => $val){
            $method = $val['method'];
            $field  = $val['header'];
            $str    = $data[$key] ?? "";
            switch($method){
                case "required" : !strlen(trim($str)) && 
                                    $this->errors[$row][] = "Missing value in {$field}";
                                  break;
                case "noSpace"  : strpos($str, " ") !== false &&
                                    $this->errors[$row][] = "{$field} should not contain any space";
                                  break;
                default         : break;
            }   
        }
        
    }
    
    /**
     * Check existing errors and throw it
     * 
     * @throws \Exception
     */
    private function checkErrors()
    {
        if(count($this->errors))
        {
           $msg = "\r\n" . implode("\r\n", array_map
                   (
                      function ($k, $v)
                       { 
                          return "$k: $v"; 
                        
                       }, 
                    array_keys($this->errors), array_values($this->errors)
                    )
                ) . "\r\n";
                       
            throw new \Exception($msg);
        }
    }
    
    
    /**
     * Print validation result
     */
    private function printReport()
    {
        if(count($this->errors))
        {
            $mask = "|%5.5s |%-30s\n";
            printf($mask, 'Row', 'Error');

            foreach ($this->errors as $key => $val)
            {
                printf($mask, $key, implode(", ", $val));
            }
        } else 
        {
            echo "File valid.\n";
        }
    }

}
