<?php
function forwardCheck($str, $n){
    $arr   = str_split($str);
    $count = 0;
    $char  = $arr[$n]; 
    if($char != '(') return "Wrong char to check#{$char}";
    for($i = $n; $i < count($arr); $i++){
        $arr[$i] == '(' ? $count++ : ($arr[$i] == ')' ? $count-- : "");
        if($count == 0) return $i; 
    }
    return 0;
}

$str = "ds(ds)(ds5657(dsds(ds)(dsds))dsds)";
echo forwardCheck($str, 2);
